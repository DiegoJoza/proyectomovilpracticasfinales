package com.example.diegojoza.primeraapp4a;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Fragmento extends AppCompatActivity implements View.OnClickListener, FrgUn.OnFragmentInteractionListener,fragdos.OnFragmentInteractionListener{
    Button botonfrag1, botonfrag2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmento);
        botonfrag1 = findViewById(R.id.frag1);
        botonfrag2 = findViewById(R.id.frag2);
        botonfrag1.setOnClickListener(this);
        botonfrag2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.frag1:
                FrgUn fragmentoUno = new FrgUn();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor,fragmentoUno);
                transactionUno.commit();
                break;
        }

        switch (v.getId()){
            case R.id.frag2:
                fragdos fragmentoDos = new fragdos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor,fragmentoDos);
                transactionDos.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
