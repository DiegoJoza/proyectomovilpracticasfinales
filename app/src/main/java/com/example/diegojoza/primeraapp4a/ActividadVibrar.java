package com.example.diegojoza.primeraapp4a;

import android.content.Context;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadVibrar extends AppCompatActivity {
    Button botonvibrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_vibrar);

        final Vibrator vibrator = (Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);

        botonvibrar = findViewById(R.id.btnVibrar);
        botonvibrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              vibrator.vibrate(600);
            }
        });

    }
}
