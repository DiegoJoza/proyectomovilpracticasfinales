package com.example.diegojoza.primeraapp4a;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ActividadMemoriaInterna extends AppCompatActivity  implements View.OnClickListener{
    TextView nombre, cedula, apellido, dato;
    Button Leer, Escribirl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_memoria_interna);
        nombre = findViewById(R.id.TxtNombreMI);
        cedula = findViewById(R.id.TxtCedulaMI);
        apellido = findViewById(R.id.TxtApellidoMI);
        dato = findViewById(R.id.TxtDatoMI);
        Leer = findViewById(R.id.btnLeerMi);
        Escribirl = findViewById(R.id.bntEscribirMi);

        Escribirl.setOnClickListener(this);
        Leer.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bntEscribirMi:
                try {
                    OutputStreamWriter escritor = new OutputStreamWriter (openFileOutput("archivo.txt", Context.MODE_APPEND));
                    escritor.write(cedula.getText().toString() +","+apellido.getText().toString() +","+nombre.getText().toString());
                    escritor.close();

                }catch (Exception ex){
                    Log.e("Archivo MI","error En el archivo de escritura");
                }
                break;
                case R.id.btnLeerMi:
                    try {
                        BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                        String datos = lector.readLine();
                        String [] listaPersonas = datos.split(";");
                        for (int i = 0; i < listaPersonas.length; i++){
                            dato.append(listaPersonas[i].split(",") [0] + listaPersonas[i].split(",") [1] + listaPersonas[i].split(",") [2]);
                        }
                        //dato.setText("");
                        lector.close();
                    }catch (Exception ex){
                    Log.e("Archivo MI", "error en la lectura del archivo"+ ex.getMessage());
        }
break;
    }
}}
